#!/usr/bin/env python3

import dataclasses
import json
import sys
import itertools
from dataclasses import dataclass, field
from enum import Enum
from pathlib import Path
from typing import Optional
from collections import Counter
from hamming import Hamming

import numpy as np
import numpy.typing as npt

# Координаты в абсолютной системе координат (не совпадает с сис. коорд для участников)
# нормализованные по длине сектора

MIN_X = 0
MAX_X = 8
ARTAG_SIZE = 6  # 4x4 на сообщение + края


class Direction(Enum):
    w = [0, -1]
    s = [0, 1]
    a = [-1, 0]
    d = [1, 0]


# y axis
dir_angles = {
    Direction.w: 0,
    Direction.a: np.pi / 2,
    Direction.d: -np.pi / 2,
    Direction.s: np.pi,
}


class Color(Enum):
    red = '#ff0000'
    green = '#00ff00'
    blue = '#0000ff'

    # should be the last box generated
    yellow = '#ffff00'


colors_dir = {
    Color.yellow: Direction.w,
    Color.red: Direction.d,
    Color.blue: Direction.s,
    Color.green: Direction.a,
}


class EnhancedJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        if isinstance(o, np.ndarray):
            return o.tolist()
        if isinstance(o, np.integer):
            return int(o)
        if isinstance(o, np.floating):
            return float(o)
        if isinstance(o, Enum):
            return o.value
        return super().default(o)


ids = Counter()
data = {'boxes': [], 'robot': None}


def check_intersection(x, y, data, dist=np.linalg.norm([1, 1])):
    for x0, y0 in itertools.chain(
        (((box.x0 + box.x1) / 2, (box.y0 + box.y1) / 2)  # середина коробки
         for box in data['boxes']),
        [(data['robot'].x, data['robot'].y)] if data['robot'] else [],
        ((box.artag.x_field, box.artag.y_field)
         for box in data['boxes'] if box.artag is not None)
    ):
        if np.linalg.norm([x0 - x, y0 - y]) < dist:
            return True
    return False


@dataclass
class Generatable:
    id: int = field(init=False)

    def __post_init__(self):
        global ids
        tname = str(type(self))
        self.id = ids[tname]
        ids[tname] += 1


@dataclass
class ARTag(Generatable):
    x: float
    y: float
    x_field: float = None
    y_field: float = None
    artag: npt.ArrayLike = None
    flip: bool = False

    def __post_init__(self):
        # generate the artag
        super().__post_init__()

        if self.artag is None:
            x, y = self.x, self.y
            bits = []

            for i in range(int(np.log2(MAX_X - MIN_X))):
                bits.append(x & 1)
                x >>= 1
            assert len(bits) == 3

            for i in range(int(np.log2(MAX_X - MIN_X))):
                bits.append(y & 1)
                y >>= 1
            assert len(bits) == 6

            h = Hamming(12)
            r = h.encode_block(bits)

            self.artag = self.bits_to_artag(r)
            # print(self.artag)

    @staticmethod
    def bits_indexes():
        for i in range(1, ARTAG_SIZE - 1):
            for j in range(1, ARTAG_SIZE - 1):
                if (i == 1 or i == ARTAG_SIZE - 2) and (j == 1 or j == ARTAG_SIZE - 2):
                    continue
                yield i, j

    @staticmethod
    def bits_to_artag(message: npt.ArrayLike):
        """
        Возвращает artag в в иде массива
        black = 1, white = 0
        """
        r = np.ones((ARTAG_SIZE, ARTAG_SIZE), dtype=np.uint8)

        r[-2, -2] = 0
        for bit, (i, j) in zip(message, ARTag.bits_indexes()):
            r[i, j] = bit

        return r

    @classmethod
    def gen(cls, data=data):
        while 1:
            x, y = np.random.randint(MIN_X, MAX_X, size=2)
            x0, y0 = x, y
            x_field, y_field = cls.get_field_coors(
                data['boxes'], data['robot'], x, y)
            if check_intersection(x_field, y_field, data):
                continue
            break

        return cls(x=x0, y=y0, x_field=x_field, y_field=y_field)

    @classmethod
    def from_field_coords(cls, x_field, y_field, boxes, robot):
        A = np.linalg.inv(xy_transform(boxes, robot))
        x, y, _ = A @ np.array([x_field, y_field, 1])

        assert int(x) == x and int(y) == y

        return cls(x=int(x), y=int(y), x_field=x_field, y_field=y_field)

    @staticmethod
    def get_field_coors(boxes, robot, x, y):
        A = xy_transform(boxes, robot)
        (x_field,
         y_field,
         _) = A @ np.array([x, y, 1])

        return x_field, y_field


def box_dist(box):
    return min(box.x0 - MIN_X, box.y0 - MIN_X, MAX_X - box.x1, MAX_X - box.y1)


def xy_transform(boxes: 'list[Box]', robot: 'Robot') -> npt.ArrayLike:
    """
    Returns transformation matrix from box-based basis to gazebo basis
    How to apply: [xn, yn, _] = TRANSFORM_MATRIX @ [x0, y0, 1]
    """

    imin = 0
    for i in range(1, len(boxes)):
        if box_dist(boxes[i]) < box_dist(boxes[imin]):
            imin = i

    bmin = boxes[imin]

    def rotate(x):
        return np.array([[np.cos(x), -np.sin(x), 0],
                         [np.sin(x), np.cos(x), 0],
                         [0, 0, 1]])

    def shift(dx, dy):
        return np.array([[1, 0, dx],
                         [0, 1, dy],
                         [0, 0, 1]])

    flip_y = np.array([[1, 0, 0],
                       [0, -1, 0],
                       [0, 0, 1]])  # flip Y axis

    if bmin.color == Color.red:
        S = shift(0.5, 0.5)
    elif bmin.color == Color.blue:
        S = shift(7.5, 0.5)
    elif bmin.color == Color.green:
        S = shift(7.5, 7.5)
    elif bmin.color == Color.yellow:
        S = shift(0.5, 7.5)

    R_COORDS = rotate(dir_angles[colors_dir[bmin.color]])

    A = S @ flip_y @ R_COORDS

    return np.round(A, 1)


@dataclass
class Box(Generatable):
    x0: float
    y0: float
    x1: float
    y1: float
    color: Color
    artag: Optional[ARTag]
    artag_direction: list  # [Δx, Δy]
    x_center_local: float = None
    y_center_local: float = None

    @classmethod
    def gen(cls, color: Color, data=data):
        while True:
            x0, y0 = np.random.randint(MIN_X, MAX_X * 2 - 2, size=2) * 0.5
            x1, y1 = x0 + 1, y0 + 1

            if check_intersection((x0 + x1) / 2, (y0 + y1) / 2, data, dist=np.linalg.norm([1, 1])):
                continue

            box_dists = {box_dist(box) for box in data['boxes']}

            box = cls(x0=x0,
                      y0=y0,
                      x1=x1,
                      y1=y1,
                      color=color,
                      artag=None,
                      artag_direction=None)

            if box_dist(box) in box_dists:
                continue

            break

        artag = None
        artag_direction = None

        if color == Color.yellow:
            # TODO: Add artag generator
            artag = ARTag.gen(data={'boxes': data['boxes'] + [box],
                                    'robot': data['robot']})
            artag_direction = np.random.choice(list(Direction))

        return cls(x0=x0,
                   y0=y0,
                   x1=x1,
                   y1=y1,
                   color=color,
                   artag=artag,
                   artag_direction=artag_direction)


@dataclass
class Robot(Generatable):
    # центр робота
    x: float
    y: float
    dir: Direction
    x_local: float = None
    y_local: float = None

    @classmethod
    def gen(cls, data=data):
        while True:
            # bottom right corner with respect to gazebo's coordinates
            x, y = (MAX_X-.5, MAX_X-.5)

            if check_intersection(x, y, data):
                raise "Cannot generate the field"
            break

        return cls(x=x, y=y, dir=np.random.choice(list(Direction)))


def set_hints(data):
    for box in data['boxes']:
        A = np.linalg.inv(xy_transform(data['boxes'], data['robot']))
        (box.x_center_local,
         box.y_center_local,
         _) = A @ np.array([(box.x0 + box.x1) / 2,
                           (box.y0 + box.y1) / 2,
                            1])
    (data['robot'].x_local, data['robot'].y_local, _) = A @ np.array([data['robot'].x,
                                                                      data['robot'].y,
                                                                      1])
    boxes = data['boxes']

    imin = 0
    for i in range(1, len(boxes)):
        if box_dist(boxes[i]) < box_dist(boxes[imin]):
            imin = i

    data['closest_box'] = boxes[imin]


if __name__ == '__main__':
    np.random.seed()
    out = Path(sys.argv[1])
    assert (out / '..').resolve().is_dir()

    for color in Color:
        data['boxes'].append(Box.gen(color=color))
    data['robot'] = Robot.gen()

    set_hints(data)

    with out.open('w') as f:
        json.dump(data, f, cls=EnhancedJSONEncoder, indent=2)
