#/usr/bin/env python3
import numpy as np
import json
import sys
from pathlib import Path
from PIL import Image, ImageDraw

SQ_SIZE = 16

def gen_pic(in_array):
    im = Image.new('1', tuple(np.array(in_array.shape) * SQ_SIZE))
    draw = ImageDraw.Draw(im)

    for i in range(in_array.shape[0]):
        for j in range(in_array.shape[1]):
            draw.rectangle([
                (SQ_SIZE * j, SQ_SIZE * i),
                (SQ_SIZE * (j + 1) - 1, SQ_SIZE * (i + 1) - 1)
            ], fill=int(not in_array[i, j]))

    return im

if __name__ == '__main__':
    inp = sys.argv[1]
    out = sys.argv[2]
    assert Path(inp).is_file()
    assert Path(out).parent.is_dir()

    with open(inp) as inp_fd:
        in_array = np.array(json.load(inp_fd)['artag'], dtype=np.byte)
        im = gen_pic(in_array)
        im.save(out)
