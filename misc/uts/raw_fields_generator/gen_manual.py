# Generates test into manual/$NAME
import json
import numpy as np
from pathlib import Path
import random

from gen import Box, Color, Robot, Direction, EnhancedJSONEncoder, MAX_X, check_intersection, ARTag, set_hints
from gen_day_by_day import gen_artag

OUTPUT = Path('day3_closed/01.json')
ARTAG = None  # generate artag from coordinates

# Координаты: range: [0, 8); должно быть равно 0 + k * .5 где k ∈ N

# Координаты координат газебо ЦЕНТРА ЯЧЕЙКИ в которую должен приехать робот
ARTAG_XY = np.array([5.5, 7.5])

# Позиция artag относительно коробки
# Задаётся так
#   w
# a s d
#
# Относительно координат gazebo
ARTAG_DIR = Direction.a
BITFLIP = False

# Координаты начала (угла) ячеек
BOX_YELLOW_START_XY = np.array([4, 4])
BOX_RED_START_XY    = np.array([5.5, 5.5])
BOX_GREEN_START_XY  = np.array([3.5, 5])
BOX_BLUE_START_XY   = np.array([4.5, 3])

# ЦЕНТР робота
ROBOT_START = np.array([7.5, 7.5])
ROBOT_DIR = Direction.s


data = {
    'robot': Robot(x=ROBOT_START[0], y=ROBOT_START[1], dir=ROBOT_DIR),
    'boxes': [
        Box(x0=BOX_RED_START_XY[0], y0=BOX_RED_START_XY[1], x1=BOX_RED_START_XY[0] + 1, y1=BOX_RED_START_XY[1] + 1,
            color=Color.red, artag=None, artag_direction=None),
        Box(x0=BOX_GREEN_START_XY[0], y0=BOX_GREEN_START_XY[1], x1=BOX_GREEN_START_XY[0] + 1, y1=BOX_GREEN_START_XY[1] + 1,
            color=Color.green, artag=None, artag_direction=None),
        Box(x0=BOX_BLUE_START_XY[0], y0=BOX_BLUE_START_XY[1], x1=BOX_BLUE_START_XY[0] + 1, y1=BOX_BLUE_START_XY[1] + 1,
            color=Color.blue, artag=None, artag_direction=None),
        Box(x0=BOX_YELLOW_START_XY[0], y0=BOX_YELLOW_START_XY[1], x1=BOX_YELLOW_START_XY[0] + 1, y1=BOX_YELLOW_START_XY[1] + 1,
            color=Color.yellow,
            artag=None,
            artag_direction=ARTAG_DIR),
    ]
}

data['boxes'][-1].artag = ARTag.from_field_coords(ARTAG_XY[0], ARTAG_XY[1], data['boxes'], data['robot'])

def is2pow(x):
    l2 = np.log2(x)
    return int(l2) == l2

if BITFLIP:
    bits = [x for i, x in enumerate(ARTag.bits_indexes()) if not is2pow(i + 1)][:-2]
    flip = random.choice(bits)
    data['boxes'][-1].artag.artag[flip] = 1 - data['boxes'][-1].artag.artag[flip]

data['boxes'][-1].artag.flip = BITFLIP

if __name__ == '__main__':
    set_hints(data)

    with open(OUTPUT, 'w') as f:
        json.dump(data, f, cls=EnhancedJSONEncoder, indent=2)
    gen_artag(data, OUTPUT.parent / (OUTPUT.stem + '.png'))
