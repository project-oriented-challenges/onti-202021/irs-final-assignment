import sys
import numpy as np
import json
from pathlib import Path

from gen import Box, Color, Robot, Direction, EnhancedJSONEncoder, MAX_X, check_intersection, set_hints
from artag_picture import gen_pic

np.random.seed()

NTESTS = 100

def check_path(out):
    out = Path(out)
    assert (out / '..').resolve().is_dir()

def gen_day1(out):
    data = {'boxes': [], 'robot': None}
    check_path(out)

    data['robot'] = Robot.gen(data=data)
    for color in Color:
        data['boxes'].append(Box.gen(color=color, data=data))

    set_hints(data)
    with open(out, 'w') as f:
        json.dump(data, f, cls=EnhancedJSONEncoder, indent=2)

    return data

def gen_day2(out):
    data = {'boxes': [], 'robot': None}
    check_path(out)

    ybox = None

    for color in Color:
        box = Box.gen(color=color, data=data)
        data['boxes'].append(box)
        if box.artag:
            ybox = box

    class RobotDay2(Robot):
        @classmethod
        def gen(cls):
            cells = [np.array([ybox.x0, ybox.y0]) + np.array(d.value) + np.array([.5, .5]) for d in Direction]
            cells = [[x, y] for [x, y] in cells if x >= .5 and y >= .5 and x <= MAX_X - .5 and y <= MAX_X - .5]
            while True:
                x, y = cells[np.random.randint(0, len(cells))]  # random corner

                if check_intersection(x, y, data=data, dist=1):
                    continue
                break

            return cls(x=x, y=y, dir=np.random.choice(list(Direction)))

    data['robot'] = RobotDay2.gen()

    set_hints(data)
    with open(out, 'w') as f:
        json.dump(data, f, cls=EnhancedJSONEncoder, indent=2)

    return data

def gen_day3(out):
    return gen_day1(out)


def gen_artag(data, fname):
    in_arr = [box.artag for box in data['boxes'] if box.artag is not None][0].artag
    nrot = np.random.randint(0, 4)
    in_arr = np.rot90(in_arr, nrot)
    pic = gen_pic(in_arr)
    pic.save(fname)

    return pic

if __name__ == '__main__':
    __dirname = Path(__file__).parent
    for i in range(NTESTS):
        data = gen_day1(__dirname / 'day1'/ f'{i:>02}.json')

        data = gen_day2(__dirname / 'day2'/ f'{i:>02}.json')
        gen_artag(data, __dirname / 'day2'/ f'{i:>02}.png')

        data = gen_day3(__dirname / 'day3'/ f'{i:>02}.json')
        gen_artag(data, __dirname / 'day3'/ f'{i:>02}.png')
