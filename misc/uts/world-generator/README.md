# Gazebo world generator

## How to run this script:

### You should be inside ```world-generator``` directory

1. Create virtual environment: 

```
python3 -m venv env
```

2. Source it: 
```
source env/bin/activate
```

3. Install requirements:
```
pip3 install -r requirements.txt
```

4. Run it
```
python3 main.py
```