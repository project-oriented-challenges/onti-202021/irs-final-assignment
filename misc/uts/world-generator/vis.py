from PIL import Image, ImageDraw

BOX_SIDE_PXLS = 80
IMG_PXLS = 800


def create_image(width: int = 800, height: int = 800) -> tuple:
    """
    Function creates image for empty maze only with outer box
    Returns tuple (Image, ImageDraw)
    """
    img = Image.new("RGB", (width, height), color="white")
    im_d = ImageDraw.Draw(img)

    dimension = 8

    for i in range(0, 10):
        for j in range(0, 10):
            if (i == 0 or i == 9) or (j == 0 or j == 9):
                x, y = i, j
                add_box(im_d, x, y)

    for i in range(0, 10):
        im_d.line((0, i * BOX_SIDE_PXLS, 800, i * BOX_SIDE_PXLS), fill="Black")
        im_d.line((i * BOX_SIDE_PXLS, 0, i * BOX_SIDE_PXLS, 800), fill = "Black")

    
    return (img, im_d)

def save_pic(img: Image, path: str):
    img.save(path)


def add_box(img_draw: ImageDraw, y, x, color="Gray"):
    img_draw.rectangle([x * BOX_SIDE_PXLS, y * BOX_SIDE_PXLS, x * BOX_SIDE_PXLS +
                        BOX_SIDE_PXLS, y * BOX_SIDE_PXLS + BOX_SIDE_PXLS], fill=color)

def add_robot(img_draw: ImageDraw, y, x, d, color="Turquoise"):
    if d == [0, -1]:
        rot = -90
    elif d == [0, 1]:
        rot = 90
    elif d == [1, 0]:
        rot = 0
    elif d == [-1, 0]:
        rot = 180

    rot += 180

    img_draw.regular_polygon(((x * BOX_SIDE_PXLS + BOX_SIDE_PXLS / 2, y * BOX_SIDE_PXLS + BOX_SIDE_PXLS / 2), 24),
                             n_sides=3, rotation=rot, fill=color)

def add_circle(img_draw: ImageDraw, y, x, color="Navy"):
    img_draw.ellipse([
        x * BOX_SIDE_PXLS, y * BOX_SIDE_PXLS,
        x * BOX_SIDE_PXLS + BOX_SIDE_PXLS, y * BOX_SIDE_PXLS + BOX_SIDE_PXLS
    ], fill=color)


# im, imd = create_image()
# add_robot(imd, 3, 2, [-1, 0])
# add_box(imd, 5, 3, "Green")

# save_pic(im, "test.png")
