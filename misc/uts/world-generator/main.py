from collections import namedtuple
import xmltodict
import json
import pprint
import copy
import math
import os
import vis
from os import walk
import shutil

# x y z, roll pitch yaw
Position = namedtuple('Position', 'x y z r p ya')
# x y z
Size = namedtuple('Size', 'x y z')

with open("world_template.xml") as template:
    PARSED_TEMPLATE = xmltodict.parse(template.read())

pp = pprint.PrettyPrinter(indent=1)


class Generator:
    BOX_SIDE = 0.6
    BOX_HEIGHT = 0.4

    def __init__(self, json_obj, txt_path: str, generate_artag=True):
        self.txt_path = txt_path
        self.VISUALIZER: tuple = vis.create_image()
        self.copied_template = copy.deepcopy(PARSED_TEMPLATE)
        self.json = json_obj
        self.generate_artag = generate_artag
        if not self.generate_artag:
            self.copied_template['sdf']['world']['include'].pop(1)
            self.copied_template['sdf']['world']['include'].pop(1)
            self.copied_template['sdf']['world']['include'].pop()

        # collects all models generated from template
        self.models = []
        self.add_json_world()
        self.copied_template['sdf']['world']['model'] = []
        self.add_outer_border(8)

    def add_json_world(self) -> None:
        boxes = self.json['boxes']
        robot = self.json['robot']

        box_size = Size(self.BOX_SIDE, self.BOX_SIDE, self.BOX_HEIGHT)

        robot['dir'] = robot['dir'][::-1]
        robot['x'], robot['y'] = robot['y'], robot['x']

        try:
            required_id = self.json['closest_box']['id']
            comment_txt = open(self.txt_path, 'w')

            comment_txt.write("CLOSEST {} box: ({}, {})\n".format(
                {"#0000ff": "Gazebo/Blue",
                 "#00ff00": "Gazebo/Green",
                 "#ff0000": "Gazebo/Red",
                 "#ffff00": "Gazebo/Yellow"}[self.json['closest_box']['color']], self.json['closest_box']['x_center_local'], self.json['closest_box']['y_center_local']))
        except:
            pass

        for box in boxes:
            name = box['id']

            # swapping coordinates
            box['x0'], box['y0'] = box['y0'], box['x0']

            pos = Position(box['x0'] * self.BOX_SIDE,
                           box['y0'] * self.BOX_SIDE, 0, 0, 0, 0)
            color = box['color']

            vis.add_box(self.VISUALIZER[1],
                        box['x0'] + 1, box['y0'] + 1, color)

            try:
                required_id = self.json['closest_box']['id']
                if required_id == name:
                    vis.add_label(self.VISUALIZER[1], box['x0'] + 1, box['y0'] + 1, str(
                        (box['x_center_local'], box['y_center_local'])), color="DarkOrange")
                else:
                    vis.add_label(self.VISUALIZER[1], box['x0'] + 1, box['y0'] + 1, str(
                        (box['x_center_local'], box['y_center_local'])), color="Black")

                local_color = {"#0000ff": "Gazebo/Blue",
                               "#00ff00": "Gazebo/Green",
                               "#ff0000": "Gazebo/Red",
                               "#ffff00": "Gazebo/Yellow"}[color]
                comment_txt.write("{} box: ({}, {})\n".format(
                    local_color, box['x_center_local'], box['y_center_local']))
            except:
                pass

            """
            example for positioning for dir = [1, 0]
            <include>
                <uri>model://artag_background</uri>
                <pose>0 0.3 0.3 1.5708 0 1.5708</pose>
            </include>
            <include>
                <uri>model://0_5</uri>
                <pose>0.005 0.3 0.275 0 0 0</pose>
            </include>
            """

            # include[1] - artag background
            # include[2] - artag model

            # if we need to add artag on top
            if box['artag'] != None and self.generate_artag:

                box['artag']['x_field'], box['artag']['y_field'] = box['artag']['y_field'], box['artag']['x_field']
                box['artag_direction'] = box['artag_direction'][::-1]

                vis.add_robot(
                    self.VISUALIZER[1], box['x0'] + 1, box['y0'] + 1, box['artag_direction'], color="black")

                bg_z = self.BOX_HEIGHT * 0.75
                # offset on 25 mm because artag is 50 mm
                tag_z = self.BOX_HEIGHT * 0.75 - 0.025

                if box['artag_direction'] == [1, 0]:
                    bg_pos = Position(
                        box['x0'] * self.BOX_SIDE + self.BOX_SIDE,
                        box['y0'] * self.BOX_SIDE + self.BOX_SIDE / 2,
                        bg_z,
                        math.pi / 2,
                        0,
                        math.pi / 2
                    )
                    tag_pos = Position(
                        bg_pos.x + 0.005,  # offset for 5 mm
                        bg_pos.y,
                        tag_z,
                        0,
                        0,
                        0
                    )
                elif box['artag_direction'] == [0, 1]:
                    bg_pos = Position(
                        box['x0'] * self.BOX_SIDE + self.BOX_SIDE / 2,
                        box['y0'] * self.BOX_SIDE + self.BOX_SIDE + 0.003,
                        bg_z,
                        - math.pi / 2,
                        0,
                        math.pi
                    )
                    tag_pos = Position(
                        bg_pos.x,
                        bg_pos.y,
                        tag_z,
                        0,
                        0,
                        math.pi / 2
                    )
                elif box['artag_direction'] == [-1, 0]:
                    bg_pos = Position(
                        box['x0'] * self.BOX_SIDE - 0.003,
                        box['y0'] * self.BOX_SIDE + self.BOX_SIDE / 2,
                        bg_z,
                        - math.pi / 2,
                        0,
                        - math.pi / 2
                    )
                    tag_pos = Position(
                        bg_pos.x,
                        bg_pos.y,
                        tag_z,
                        0,
                        0,
                        math.pi
                    )
                elif box['artag_direction'] == [0, -1]:
                    bg_pos = Position(
                        box['x0'] * self.BOX_SIDE + self.BOX_SIDE / 2,
                        box['y0'] * self.BOX_SIDE - 0.003,
                        bg_z,
                        - math.pi / 2,
                        0,
                        0
                    )
                    tag_pos = Position(
                        bg_pos.x,
                        bg_pos.y,
                        tag_z,
                        0,
                        0,
                        - math.pi / 2
                    )

                self.copied_template['sdf']['world']['include'][1]['pose'] = "{} {} {} {} {} {}".format(
                    bg_pos.x, bg_pos.y, bg_pos.z, bg_pos.r, bg_pos.p, bg_pos.ya
                )
                self.copied_template['sdf']['world']['include'][2]['pose'] = "{} {} {} {} {} {}".format(
                    tag_pos.x, tag_pos.y, tag_pos.z, tag_pos.r, tag_pos.p, tag_pos.ya
                )

                self.copied_template['sdf']['world']['include'][5]['pose'] = "{} {} {} {} {} {}".format(
                    box['artag']['x_field'] *
                    self.BOX_SIDE, box['artag']['y_field'] *
                    self.BOX_SIDE, 0, 0, 0, 0
                )

                vis.add_circle(self.VISUALIZER[1],
                               box['artag']['x_field'] + 0.5, box['artag']['y_field'] + 0.5, "Navy")

            self.models.append(
                self.get_box(
                    name,
                    pos,
                    box_size,
                    color
                )
            )

        if robot['dir'] == [1, 0]:
            yaw = 0
        elif robot['dir'] == [0, 1]:
            yaw = math.pi / 2
        elif robot['dir'] == [-1, 0]:
            yaw = math.pi
        elif robot['dir'] == [0, -1]:
            yaw = - math.pi / 2
        else:
            raise Exception("unknown direction")

        vis.add_robot(self.VISUALIZER[1], robot['x'] + 0.5,
                      robot['y'] + 0.5, robot['dir'], "ForestGreen")

        robot_pos = Position(robot['x'] * self.BOX_SIDE,
                             robot['y'] * self.BOX_SIDE, 0, 0, 0, yaw)

        self.copied_template['sdf']['world']['include'][0]['pose'] = "{} {} {} {} {} {}".format(
            robot_pos.x, robot_pos.y, robot_pos.z, robot_pos.r, robot_pos.p, robot_pos.ya
        )

        try:
            required_id = self.json['closest_box']['id']
            comment_txt.close()
        except:
            pass

    def add_outer_border(self, dimension: int) -> None:
        """
        function generates border with given size of boxes
        """

        idx = 0
        for i in range(-1, 1 + dimension):
            for config in [
                [0, -1 * self.BOX_SIDE, i * self.BOX_SIDE],
                [1, dimension * self.BOX_SIDE, i * self.BOX_SIDE],
                [0, i * self.BOX_SIDE, -1 * self.BOX_SIDE],
                    [0, i * self.BOX_SIDE, dimension * self.BOX_SIDE]]:

                self.models.append(
                    self.get_box(
                        "{}_{}".format(config[0], idx),
                        Position(config[1], config[2], 0, 0, 0, 0),
                        Size(self.BOX_SIDE, self.BOX_SIDE, self.BOX_HEIGHT)
                    )
                )
                idx += 1

    def get_box(self, name: str,  pos: Position, size: Size, color: str = "Gazebo/Grey"):
        # get box xml from template
        template = copy.deepcopy(PARSED_TEMPLATE['sdf']['world']['model'])
        template['@name'] = name

        pos_str = "{} {} {} {} {} {}".format(
            pos.x + size.x / 2, pos.y + size.y / 2, pos.z + size.z / 2, pos.r, pos.p, pos.ya)
        size_str = "{} {} {}".format(size.x, size.y, size.z)

        template['pose'] = pos_str
        template['link']['collision']['geometry']['box']['size'] = size_str
        template['link']['visual']['geometry']['box']['size'] = size_str

        # if color of the box is not usual
        if color != "Gazebo/Grey":
            color = {"#0000ff": "Gazebo/Blue",
                     "#00ff00": "Gazebo/Green",
                     "#ff0000": "Gazebo/Red",
                     "#ffff00": "Gazebo/Yellow"}[color]

        # update color of box
        template['link']['visual']['material']['script']['name'] = color

        return template.copy()

    def save_world(self, filename, idx: int):
        """
        function saves generated world
        """

        if self.generate_artag:
            self.copied_template['sdf']['world']['include'][2]['uri'] = "model://0{}".format(
                idx) if idx <= 9 else "model://{}".format(idx)

        for model in self.models:
            self.copied_template['sdf']['world']['model'].append(model)

        vis.save_pic(self.VISUALIZER[0], filename + ".png")

        with open(filename, 'w') as output_file:
            output_file.write(xmltodict.unparse(
                self.copied_template, pretty=True))


def generate_world(folder: str, id_input: int, output_folder: str, generate_artag=True) -> None:
    name = "0{}.json".format(
        id_input) if id_input <= 9 else "{}.json".format(id_input)
    with open(folder + name, 'r') as raw_field:
        world = Generator(json.load(raw_field), output_folder +
                          name + ".txt", generate_artag)
        world.save_world(output_folder + "{}.world".format(id_input), id_input)


if __name__ == "__main__":
    # /home/leo/catkin_ws/src/gazebo_models/ar_tags/scripts

    folders_to_generate = [
        "day1/",
        "day1_closed/",
        "day1_open/",
        "day2/",
        "day2_closed/",
        "day2_open/",
        "day3/",
        "manual/"
    ]

    base_path = "../raw_fields_generator/"

    for folder in folders_to_generate:
        try:
            shutil.rmtree(folder)
            os.mkdir(folder)
        except:
            os.mkdir(folder)

        _, _, filenames = next(walk(base_path + folder))

        number_of_jsons = 0
        json_names = []
        for filename in filenames:
            if filename.endswith(".json"):
                number_of_jsons += 1
                json_names.append(filename.split('.')[0])

        os.system('python3 /home/leo/catkin_ws/src/gazebo_models/ar_tags/scripts/generate_markers_model.py -i /home/leo/Documents/projects/ros/irs-final-assignment/misc/uts/raw_fields_generator/{} -g /home/leo/Documents/projects/ros/irs-final-assignment/misc/uts/world-generator/{} -s 50'.format(folder, folder))

        for i in json_names:
            generate_world(base_path + folder, int(i), folder,
                           True if (folder not in ["day1/", "day1_closed/", "day1_open/"]) else False)

    # # generate day 1
    # for i in range(100):
    #     generate_world("../raw_fields_generator/day1/", i, "day1/", False)

    # # generate day 2
    # try:
    #     os.system('python3 /home/leo/catkin_ws/src/gazebo_models/ar_tags/scripts/generate_markers_model.py -i /home/leo/Documents/projects/ros/irs-final-assignment/misc/uts/raw_fields_generator/day2 -g /home/leo/Documents/projects/ros/irs-final-assignment/misc/uts/world-generator/day2 -s 50')
    # except:
    #     print("error artag gen")
    # for i in range(100):
    #     generate_world("../raw_fields_generator/day2/", i, "day2/")

    # # generate day 3
    # try:
    #     os.system('python3 /home/leo/catkin_ws/src/gazebo_models/ar_tags/scripts/generate_markers_model.py -i /home/leo/Documents/projects/ros/irs-final-assignment/misc/uts/raw_fields_generator/day3 -g /home/leo/Documents/projects/ros/irs-final-assignment/misc/uts/world-generator/day3 -s 50')
    # except:
    #     print("error artag gen")
    # for i in range(100):
    #     generate_world("../raw_fields_generator/day3/", i, "day3/")

    # # generate day 3
    # try:
    #     os.system('python3 /home/leo/catkin_ws/src/gazebo_models/ar_tags/scripts/generate_markers_model.py -i /home/leo/Documents/projects/ros/irs-final-assignment/misc/uts/raw_fields_generator/manual -g /home/leo/Documents/projects/ros/irs-final-assignment/misc/uts/world-generator/manual -s 50')
    # except:
    #     print("error artag gen")
    # for i in range(2):
    #     generate_world("../raw_fields_generator/manual/", i, "manual/")
