#!/usr/bin/env python3

import time
import os
import signal
from subprocess import run, Popen, PIPE, DEVNULL, TimeoutExpired
from rospy.exceptions import ROSInitException
from contextlib import contextmanager
from robot_library.robot import Robot
from pathlib import Path

r = range(1, 14)
TIME_LIMIT = 5 * 60 + 10

__dirname = str(Path(__file__).parent)
GAZEBO = f'{__dirname}/participant_solutions/irs202100'
def GAZEBO_CMD(name): return ['bash', 'sh/run_simulation.sh', name]


RESET_CMD = ['bash', f'{__dirname}/participant_solutions/irs202100/sh/reset.sh']
STOP_TIME = 30
TEST_NAMES = ['1']

PARTICIPANT_STOP_TIME = 3

RESETS = 5
RESET_TIMEOUT = .3

TEST_AFTER_TIME = 10


def reset():
    run(RESET_CMD, stderr=DEVNULL, stdout=DEVNULL)
    for i in range(0, RESETS - 1):
        time.sleep(RESET_TIMEOUT)
        run(RESET_CMD, stderr=DEVNULL, stdout=DEVNULL)


def stop(p: Popen, stop_time=STOP_TIME):
    try:
        os.killpg(os.getpgid(p.pid), signal.SIGINT)
        try:
            p.wait(timeout=stop_time)
        except TimeoutExpired:
            os.killpg(os.getpgid(p.pid), signal.SIGTERM)
    except ProcessLookupError:
        pass
    finally:
        p.wait()


@contextmanager
def gazebo(test_name: str):
    os.chdir(GAZEBO)
    with Popen(GAZEBO_CMD(test_name),
               preexec_fn=os.setsid,
               stdout=DEVNULL,
               stderr=DEVNULL) as p:
        print(f"===== STARTING GAZEBO ON TEST {test_name}... =====")
        time.sleep(1)  # to prevent race condition with robot.time()

        try:
            while True:
                try:
                    robot = Robot()
                    t = robot.time()
                    print(f'sim_time: {t}')
                    if t > 1:
                        yield p
                        break
                except (ROSInitException, ConnectionRefusedError):
                    pass

                time.sleep(1)

        finally:
            print("===== STOPPING GAZEBO... =====")
            stop(p)


if __name__ == '__main__':
    for test_n in TEST_NAMES:
        with gazebo(test_n) as g:
            for i in r:
                print('\n'*3)
                d = f'{__dirname}/participant_solutions/irs2021{i:>02}'
                print(f'--- setting directory: {d} ---')
                os.chdir(d)

                print('resetting')
                reset()

                with Popen(['python3', 'main.py'],
                           #    stdout=PIPE, stderr=PIPE,   # should not be included, will result in desync of the output
                           preexec_fn=os.setsid) as p:
                    robot = Robot()
                    while True:
                        t = robot.time()
                        if t >= TIME_LIMIT:
                            break

                        retcode = p.poll()
                        if retcode is not None:
                            break

                        time.sleep(1)

                    stop(p, stop_time=PARTICIPANT_STOP_TIME)

                time.sleep(TEST_AFTER_TIME)
