import sys
from robot_library.robot import *
import cv2
import numpy as np
import math
import time as tm
import hamming

# Shortcut methods
pi = math.pi
robot = Robot()
laser = robot.getLaser()

# This list will be used after detection of all boxes to store their positions and colors
# Format: ((x,y),color)
box_list_colored = []

# This list will store a 2D map of the field with resiolution equal to one semisector
# 0 - empty space
# 2 - walls
# 3 - box centers
# Every element of this map represents a cross of grid lines, NOT A SECTOR!
# (= 0.3 in Gazebo > GUI > Grid)!
map = []

# Fill the map with zeros
for i in range(0, 19):
    map.append([])
    for j in range(0, 19):
        map[i].append(0)

# Build walls around the area
for i in range(0, 19):
    map[0][i] = 2
    map[17][i] = 2
    map[i][0] = 2
    map[i][17] = 2
    map[1][i] = 2
    map[18][i] = 2
    map[i][1] = 2
    map[i][18] = 2

# Robot's start position in map coordinates
# measured in semisectors
# To use in local coordinates(as in the NTI task day 2), subtract from 16
locX = 16
locY = 16
# Rotation
# 0-UP, 1-RIGHT 2-DOWN 3-LEFT
rotation = 3
# Global coordinates calculated after all detections
X = -1
Y = -1
# Global finish coordinates after ARTAG reading
XArTag = -1
YArTag = -1


# Update coordinates after forward movement
def updateCoordinates(dist):
    global locX
    global locY
    global rotation
    if rotation == 3:
        locX -= dist
    elif rotation == 1:
        locX += dist
    elif rotation == 0:
        locY -= dist
    elif rotation == 2:
        locY += dist


# Read the color of the box
def colorDecoder():
    # retreive the frame from robot's camera
    frame = robot.getImage()
    # Translate the frame into HSV colorspace
    hsv = cv2.cvtColor(frame, cv2.COLOR_RGB2HSV)
    # Get the pixel in the middle of the frame
    pix = hsv[360][640]
    # Compare the HUE with all the four colors
    redp = min((abs(pix[0] - 0)), (abs(pix[0] - 179)))
    greenp = (abs(pix[0] - 60))
    bluep = (abs(pix[0] - 120))
    yellowp = (abs(pix[0] - 30))
    # Return the right color
    if (redp < greenp and redp < bluep and redp < yellowp):
        return "red"
    elif (greenp < redp and greenp < bluep and greenp < yellowp):
        return "green"
    elif (bluep < redp and bluep < greenp and bluep < yellowp):
        return "blue"
    elif (yellowp < redp and yellowp < bluep and yellowp < greenp):
        return "yellow"


# function for detecting the ARTag on the picture
def cameraDecode():
    global XArTag
    global YArTag
    robot.sleep(0.1)
    # retreive the frame from robot's camera
    frame = robot.getImage()
    # Translate the frame into HSV colorspace
    hsv = cv2.cvtColor(frame, cv2.COLOR_RGB2HSV)

    # define range of color in HSV
    lower = np.array([0, 127, 0])
    upper = np.array([255, 255, 255])

    # ARTag is the darkest entity on the map. To find it, it is enough to threshold the HSV image

    # Threshold the HSV image to get only specific colors
    mask = cv2.inRange(hsv, lower, upper)
    hsv[mask > 0] = (0, 0, 255)

    lower = np.array([0, 0, 10])
    upper = np.array([255, 255, 255])

    # Threshold the HSV image to get only specific colors
    mask = cv2.inRange(hsv, lower, upper)
    mask = mask[:600][:]

    # Here we find the corners of ARTag
    top = 10000
    bottom = 0
    left = 10000
    right = 0
    for i in range(len(mask)):
        for j in range(len(mask[1])):
            if (mask[i][j] < 100):
                if j < left:
                    left = j
                if j > right:
                    right = j
                if i < top:
                    top = i
                if i > bottom:
                    bottom = i

    # If we have found anything
    if (top != 10000 or bottom != 0):

        # Extract Region of Interest from original
        artagart = mask[top:bottom, left:right]

        w = abs(right - left)
        h = abs(bottom - top)
        semistepw = w / 12
        semisteph = h / 12
        artag = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

        # From the extracted ARTag image, get the martix 4x4
        for i in range(0, 4):
            for j in range(0, 4):
                if artagart[math.floor(i * semisteph * 2 + 3 * semisteph)][
                    math.floor(j * semistepw * 2 + 3 * semistepw)] > 0:
                    artag[i][j] = 1
        # Rotate the matrix until we get a right orientation
        rots = 0
        while artag[3][3] != 1:
            rots += 1
            artag = list(zip(*artag[::-1]))
            if (rots > 4):
                # Error in finding ARTag
                break

        #Empty line of bits
        artagLine = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        #Fill the list of bits
        index = 0
        for z in range(0, 4):
            for z2 in range(0, 4):
                if not ((z == 0 or z == 3) and (z2 == 0 or z2 == 3)):
                    artagLine[index] = artag[z][z2]
                    index += 1

        # applying hamming code
        h = hamming.Hamming(12)
        detectedData = h.decode_block(np.array(artagLine))

        XArTag = detectedData[0] * 1 + detectedData[1] * 2 + detectedData[2] * 4
        YArTag = detectedData[3] * 1 + detectedData[4] * 2 + detectedData[5] * 4
        # Return 2 - ARTag was found!
        return 2
    else:
        # Return 1 - ARTag was NOT found =(
        return 1

# This method translates cartesian coordinates to polar
def cart2pol(x, y):
    rho = np.sqrt(x ** 2 + y ** 2)
    phi = np.arctan2(y, x)
    return (rho, phi)


# This method translates polar coordinates to cartesian
def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)

# This method translates HSV to RGB (Thanks StackOverflow!!!)
def hsv_to_rgb(h, s, v):
    if s == 0.0: return (v, v, v)
    i = int(h * 6.)
    f = (h * 6.) - i;
    p, q, t = v * (1. - s), v * (1. - s * f), v * (1. - s * (1. - f));
    i %= 6
    if i == 0: return (v, t, p)
    if i == 1: return (q, v, p)
    if i == 2: return (p, v, t)
    if i == 3: return (p, q, v)
    if i == 4: return (t, p, v)
    if i == 5: return (v, p, q)

# This method receives an index of laser sensor cropped [40:-40] and returns the angle
# of this sensor relative to robot's rotation angle
def get_deg_by_index(index):
    return math.radians((index + 40) * 240 / 684 - 240 / 2)

# This method finds distance from line to point.
# PT1, PT2 -- line points
# PT3 -- point to calculate the distance to
def distance_line_to_point(pt1, pt2, pt3):
    p1 = np.array(pt1)
    p2 = np.array(pt2)
    p3 = np.array(pt3)
    return np.linalg.norm(np.cross(p2 - p1, p1 - p3)) / np.linalg.norm(p2 - p1)

# Standard method of finding distance between two points
# pt1=(x,y) pt2=(x,y)
def distance(pt1, pt2):
    d1 = pt1[0] - pt2[0]
    d2 = pt1[1] - pt2[1]
    return math.sqrt(d1 * d1 + d2 * d2)

# A wrapper method for the previous one, used to calculate the line length
def line_length(l):
    return distance(l[0], l[1])

# This method uses laser sensors to find boxes on the field
# Returns a list of center points of boxes in cm
def raw_laser_scan():
    robot_direction = robot.getDirection()

    # Debug Image
    img = np.zeros((1000, 1000, 3), np.uint8)

    laser = robot.getLaser()
    lv = laser.get('values')[40:len(laser.get('values')) - 40]
    # First step: find lines among all the received points
    lines_det = []
    cur_line_x = []
    cur_line_y = []
    # This 'for' cycle works like that:
    # We iterate through all the points, transform their coordinates to (x,y)
    # While doing this we populate the list of points which will represent a line later
    # If this list has more than 2 points, start checking when the next point in this list
    # will not fit on a line we built using least squares method using all previous points in this list
    # When that happens, we put the first and the last point on this list in a tuple and save this
    # tuple as a line
    for i in range(len(lv)):
        # Get the distance and the absolute angle of a point
        c_dir = get_deg_by_index(i) + robot_direction
        c_val = lv[i]
        # Translate to cartesian coordinates
        x, y = pol2cart(c_val, c_dir - math.pi / 2)
        # To prevent shift of the picture while robot rotates,
        # Subtract the laser sensor location on the robot relative to robot's wheels, 18cm
        # Later, in our experiments using a very small grid we found that actual sensor position
        # is 15.5 cm, but... Don't touch if works
        xc, yc = pol2cart(0.18, robot_direction - math.pi / 2 + math.pi)
        x -= xc
        y -= yc
        # Meters to centimeters
        x = x * 100
        y = -y * 100
        # if we see anything
        if (c_val != float('inf')):
            #draw a debug circle on a debug image
            cv2.circle(img, (int(x) + 500
                             , int(y) + 500), 1, (255, 255, 255), thickness=2, lineType=8, shift=0)

            if len(cur_line_x) < 3:
                cur_line_x.append(x)
                cur_line_y.append(y)
            else:
                if len(cur_line_x) >= 3:
                    # least squares method
                    m, b = np.polyfit(cur_line_x, cur_line_y, 1)
                    p1 = (0, b)
                    p2 = (10, 10 * m + b)
                    # if the new point is on the same line, append it to this line
                    # else, save the current line's first and last point and start a new line
                    if distance_line_to_point(p1, p2, (x, y)) < 1:
                        cur_line_x.append(x)
                        cur_line_y.append(y)
                    else:
                        save = True
                        for i in range(1, len(cur_line_x)):
                            if distance((cur_line_x[i], cur_line_y[i]), (cur_line_x[i - 1], cur_line_y[i - 1])) > 30:
                                save = False
                        if save:
                            fin_line = ((cur_line_x[0], cur_line_y[0]), (cur_line_x[-1], cur_line_y[-1]))
                            lines_det.append(fin_line)
                        cur_line_x = []
                        cur_line_y = []
    if len(cur_line_x) > 0:
        fin_line = ((cur_line_x[0], cur_line_y[0]), (cur_line_x[-1], cur_line_y[-1]))
        lines_det.append(fin_line)

    # Find boxes among these lines
    boxes = []
    # Here, we iterete throug all the lines we found
    for line in lines_det:
        # Draw a line on the debug image
        cv2.line(img, (int(line[0][0]) + 500, int(line[0][1]) + 500),
                 (int(line[1][0]) + 500, int(line[1][1]) + 500), (0, 0, 255),
                 thickness=2)
        # If it seems like this line has a length of N cubes,
        # we split the line on N equal segments
        subcubes = line_length(line) / 60
        if abs((subcubes + 0.5) % 1 - 0.5) < 0.2:
            subcubes = round(subcubes)
            for sub in range(1, subcubes + 1):
                # There we split the line on segments
                d1 = line[1][0] - line[0][0]
                d2 = line[1][1] - line[0][1]
                p1 = (line[0][0] + d1 * (sub - 1) / subcubes, line[0][1] + d2 * (sub - 1) / subcubes)
                p2 = (line[0][0] + d1 * (sub) / subcubes, line[0][1] + d2 * (sub) / subcubes)
                # draw some debug shapes
                cv2.circle(img, (int(p1[0]) + 500, int(p1[1]) + 500), 10, (255, 255, 255), thickness=2, lineType=8,
                           shift=0)
                cv2.circle(img, (int(p2[0]) + 500, int(p2[1]) + 500), 10, (255, 255, 255), thickness=2, lineType=8,
                           shift=0)
                lt = (p1, p2)
                cv2.line(img, (int(lt[0][0]) + 500, int(lt[0][1]) + 500),
                         (int(lt[1][0]) + 500, int(lt[1][1]) + 500), (0, 255, 0),
                         thickness=2)
                ltd1 = p2[0] - p1[0]
                ltd2 = p2[1] - p1[1]
                # Here we check is the line vertical or horizontal
                if abs(lt[0][0] - lt[1][0]) > abs(lt[0][1] - lt[1][1]):
                    # Horizontal line
                    # If the line is above us, the center of the box is above the line
                    if (lt[0][1] < -15):
                        boxes.append((p1[0] + ltd1 / 2, p1[1] + ltd2 / 2 - 30))
                    else:
                        boxes.append((p1[0] + ltd1 / 2, p1[1] + ltd2 / 2 + 30))
                else:
                    # Vertical line
                    # If the line is on the right side of us, the center of the box is on the right too
                    if (lt[0][0] < -15):
                        boxes.append((p1[0] + ltd1 / 2 - 30, p1[1] + ltd2 / 2))
                    else:
                        boxes.append((p1[0] + ltd1 / 2 + 30, p1[1] + ltd2 / 2))
                if len(boxes) > 1 and distance(boxes[-1], boxes[-2]) < 20:
                    del boxes[-1]
    # Draw all the boxes on debug image
    for box in boxes:
        cv2.rectangle(img, (int(box[0]) - 30 + 500, int(box[1]) - 30 + 500),
                      (int(box[0]) + 30 + 500, int(box[1]) + 30 + 500), (255, 0, 0), 3)
    # Show the debug image
    # uncomment to enable
    #for i in range(10):
    #    cv2.imshow('image', img)
    #    cv2.waitKey(1)
    return boxes

# Get the distance using 22 lasers in front of our robot
# The result is the nearest measure
# Also, it adds 0.155. This is a position of the laser sensor relative to robot wheels
def get_forward_distance_min():
    laser = robot.getLaser()
    lv = laser.get('values')[40:len(laser.get('values')) - 40]
    min_dist = 10000
    for i in range(len(lv) // 2 - 11, len(lv) // 2 + 11):
        if lv[i] < min_dist:
            min_dist = lv[i]
    return min_dist + 0.155

# This class's name is kinda understandable
class Movement:
    # Dist is the amount of semisectors we gonna move forward
    def forward(self, dist):
        target_dir = 0
        if rotation == 0:
            target_dir = np.pi
        if rotation == 1:
            target_dir = np.pi / 2
        if rotation == 2:
            target_dir = 0
        if rotation == 3:
            target_dir = -np.pi / 2
        current_dir = robot.getDirection()
        current_dist = get_forward_distance_min()
        dist_in_semisectors = current_dist / 0.3
        
        # Used to correct robot's position knowing that the distance is always a multiple of 0.3
        dist_correction = 0.3 * round(dist_in_semisectors) - current_dist
        
        target_dist = current_dist - 0.3 * dist + dist_correction
        speed_limit = 0
        dist_diff = 10000
        while abs(dist_diff) > 0.001:
            current_dir = robot.getDirection()
            
            # This thing descended to us from above
            # nobody knows how this works, but it does!!!
            # It calculates the difference in rotation ignoring the sensor value jumps
            e = (target_dir - current_dir + np.pi * 5) % (np.pi * 2) - (np.pi)
            
            # Slow acceleration to prevent drifting
            speed_limit += 0.05
            
            # never go faster than 1
            speed_limit = min(speed_limit, 1)
            # This is the distance left
            dist_diff = get_forward_distance_min() - target_dist
            # This is the linear speed calculated using speed limit and proportional control
            lin_speed = max(-speed_limit, min(speed_limit, dist_diff * 2.5))
            # This is a proportional controller used to correct our direction during the movement
            rot_speed = -e * 2
            
            robot.setVelosities(lin_speed, rot_speed)
            robot.sleep(0.01)
        robot.setVelosities(0, 0)
        updateCoordinates(dist)
    
    # Dir is the direction of movement
    def turn(self, dir):
        global rotation
        if dir == 1:
            rotation += 1
            rotation %= 4
        elif dir == -1:
            rotation += 3
            rotation %= 4
        target_dir = 0
        if rotation == 0:
            target_dir = np.pi
        if rotation == 1:
            target_dir = np.pi / 2
        if rotation == 2:
            target_dir = 0
        if rotation == 3:
            target_dir = -np.pi / 2
        
        # Same way as in forward function, but we use the speed limit with our direction control
        current_dir = robot.getDirection()
        speed_limit = 0
        while (abs(target_dir - current_dir) > 0.0001):
            current_dir = robot.getDirection()
            e = (target_dir - current_dir + np.pi * 5) % (np.pi * 2) - (np.pi)
            speed_limit += 0.1
            speed_limit = min(speed_limit, 1.5)
            speed = max(-speed_limit, min(speed_limit, -e * 2.5))
            robot.setVelosities(0, speed)
            robot.sleep(0.01)
        robot.setVelosities(0, 0)

# Initalize our class
mov = Movement()

# This function puts boxes list retrieved from raw_laser_scan() to our 2D map
def raw_to_map(raw_boxes_list):
    # We iterate through boxes
    for it in raw_boxes_list:
        # If this is not a part of a wall
        if int(round(it[0] / 30) + locX) >= 2 and int(round(it[1] / 30) + locY) >= 2 and int(
                round(it[0] / 30) + locX) <= 16 and int(round(it[1] / 30) + locY) <= 16:
            # Update the map!
            map[int(round(it[0] / 30) + locX)][int(round(it[1] / 30) + locY)] = 3
            map[int(round(it[0] / 30) + locX) - 1][int(round(it[1] / 30) + locY) - 1] = 2
            map[int(round(it[0] / 30) + locX) - 1][int(round(it[1] / 30) + locY)] = 2
            map[int(round(it[0] / 30) + locX) - 1][int(round(it[1] / 30) + locY) + 1] = 2
            map[int(round(it[0] / 30) + locX) + 1][int(round(it[1] / 30) + locY) - 1] = 2
            map[int(round(it[0] / 30) + locX) + 1][int(round(it[1] / 30) + locY)] = 2
            map[int(round(it[0] / 30) + locX) + 1][int(round(it[1] / 30) + locY) + 1] = 2
            map[int(round(it[0] / 30) + locX)][int(round(it[1] / 30) + locY) - 1] = 2
            map[int(round(it[0] / 30) + locX)][int(round(it[1] / 30) + locY) + 1] = 2

# Counts the amount of boxes on the map
def get_box_amount():
    box_count = 0
    for line in map:
        for val in line:
            if val == 3:
                box_count += 1
    return box_count

# Stores the location and direction of an entity
class Position:
    x = 0
    y = 0
    rotation = 0

    def __init__(self, x, y, rotation):
        self.x = x
        self.y = y
        self.rotation = rotation

# finding the shortest path - Dijkstra algorithm
def dijkstra(finX, finY):
    global locX
    global locY
    global rotation
    infinity = 100000
    s = list()
    #initializing parents of a previous positions
    # and minLength to the positions on map
    pos = Position(locX, locY, rotation)
    parentPos = Position(-1, -1, -1)
    s.append(pos)
    minLenghts = []
    for i in range(0, 19):
        minLenghts.append([])
        for j in range(0, 19):
            minLenghts[i].append(infinity)
    parents = []
    for i in range(0, 19):
        parents.append([])
        for j in range(0, 19):
            parents[i].append(parentPos)
    minLenghts[locX][locY] = 0
    #finding a shortest path
    pathWord = ""
    if int(finX) > 0 and int(finX) < 19 and int(finY) > 0 and int(finY) < 19:
        if map[int(finX)][int(finY)] == 0:
            while len(s):
                #popping current position
                newPos = s.pop()

                #variable add_more_weight_to_turns used to add more weight to turns
                #to create a better path
                add_more_weight_to_turns = 0

                #adding position in left side
                if newPos.rotation == 1:
                    add_more_weight_to_turns = 2
                elif newPos.rotation == 3:
                    add_more_weight_to_turns = 0
                else:
                    add_more_weight_to_turns = 1
                if map[newPos.x - 1][newPos.y] == 0 and minLenghts[newPos.x - 1][newPos.y] > minLenghts[newPos.x][
                    newPos.y] + add_more_weight_to_turns:
                    minLenghts[newPos.x - 1][newPos.y] = minLenghts[newPos.x][newPos.y] + add_more_weight_to_turns
                    s.append(Position(newPos.x - 1, newPos.y, 3))
                    parents[newPos.x - 1][newPos.y] = newPos
                if newPos.rotation == 1:
                    add_more_weight_to_turns = 0
                elif newPos.rotation == 3:
                    add_more_weight_to_turns = 2
                else:
                    add_more_weight_to_turns = 1
                # adding position in right side
                if map[newPos.x + 1][newPos.y] == 0 and minLenghts[newPos.x + 1][newPos.y] > minLenghts[newPos.x][
                    newPos.y] + add_more_weight_to_turns:
                    minLenghts[newPos.x + 1][newPos.y] = minLenghts[newPos.x][newPos.y] + add_more_weight_to_turns
                    s.append(Position(newPos.x + 1, newPos.y, 1))
                    parents[newPos.x + 1][newPos.y] = newPos
                if newPos.rotation == 2:
                    add_more_weight_to_turns = 0
                elif newPos.rotation == 0:
                    add_more_weight_to_turns = 2
                else:
                    add_more_weight_to_turns = 1
                #adding position in "down" side
                if map[newPos.x][newPos.y + 1] == 0 and minLenghts[newPos.x][newPos.y + 1] > minLenghts[newPos.x][
                    newPos.y] + add_more_weight_to_turns:
                    minLenghts[newPos.x][newPos.y + 1] = minLenghts[newPos.x][newPos.y] + add_more_weight_to_turns
                    s.append(Position(newPos.x, newPos.y + 1, 2))
                    parents[newPos.x][newPos.y + 1] = newPos
                if newPos.rotation == 0:
                    add_more_weight_to_turns = 0
                elif newPos.rotation == 2:
                    add_more_weight_to_turns = 2
                else:
                    add_more_weight_to_turns = 1
                # adding position in "up" side
                if map[newPos.x][newPos.y - 1] == 0 and minLenghts[newPos.x][newPos.y - 1] > minLenghts[newPos.x][
                    newPos.y] + add_more_weight_to_turns:
                    minLenghts[newPos.x][newPos.y - 1] = minLenghts[newPos.x][newPos.y] + add_more_weight_to_turns
                    s.append(Position(newPos.x, newPos.y - 1, 0))
                    parents[newPos.x][newPos.y - 1] = newPos

            #if we found a path - distance should not be infinity
            if minLenghts[finX][finY] != infinity:
                # take all parents to make a path
                path = list()
                tempPos = Position(finX, finY, 0)
                while tempPos.x != -1 and tempPos.y != -1:
                    path.append(tempPos)
                    tempPos = parents[tempPos.x][tempPos.y]
                prevPoint = path.pop()
                temp_rot = rotation
                prev_temp_rot = rotation
                #creating a path by parents
                while len(path):
                    newPoint = path.pop()
                    if newPoint.x > prevPoint.x:
                        temp_rot = 1
                    if newPoint.x < prevPoint.x:
                        temp_rot = 3
                    if newPoint.y > prevPoint.y:
                        temp_rot = 2
                    if newPoint.y < prevPoint.y:
                        temp_rot = 0
                    if temp_rot - prev_temp_rot == 1 or temp_rot - prev_temp_rot == -3:
                        pathWord += "R"
                    elif temp_rot - prev_temp_rot == -1 or temp_rot - prev_temp_rot == 3:
                        pathWord += "L"
                    elif abs(temp_rot - prev_temp_rot) == 2:
                        pathWord += "RR"
                    pathWord += "F"
                    prevPoint = newPoint
                    prev_temp_rot = temp_rot
    return pathWord

# Find the box which is nearest to the wall and return it's coordinates using our 2D map
def get_side_box():
    res_box = None
    minDist = 10000
    # Iterate through map
    for i in range(0, 19):
        for j in range(0, 19):
            if map[i][j] == 3:
                if i - 2 >= 0 and i - 2 < minDist:
                    minDist = i - 2
                    res_box = (i, j)
                elif j - 2 >= 0 and j - 2 < minDist:
                    minDist = j - 2
                    res_box = (i, j)
                elif 18 - i >= 0 and 18 - i < minDist:
                    minDist = 18 - i
                    res_box = (i, j)
                elif 18 - j >= 0 and 18 - j < minDist:
                    minDist = 18 - j
                    res_box = (i, j)

    return res_box

# Move the robot to specified coordinates finding the path using Dijkstra's algorithm
# 'rebuild' value shows if we need to stop, scan the area and recalculate the path every turn
# used, for example, when we still didn't find all the boxes
# 'start' value shows if we need to stop moving when we detect all the 4 boxes on the map
def move_by_path(coordinates_temp, rebuild, start=False):
    x = coordinates_temp[0]
    y = coordinates_temp[1]
    path = dijkstra(x, y)
    #if we found a path we go through it
    while len(path):
        #if we in a start and found 4 boxes then return
        if start and get_box_amount() >= 4:
            return
        #if we not rebuild a map then we can move forward to the different count of semisectors
        if not rebuild:
            forward_count = 0
            while len(path) and (path[0] == 'F'):
                forward_count += 1
                path = path[1:]

            if forward_count:
                mov.forward(forward_count)
        else:
            #else we can only go by one semisector
            if len(path):
                if path[0] == 'F':
                    mov.forward(1)
        #if we cannot go forward then it can be maybe turns to the left or right
        if len(path):
            if path[0] == 'L':
                mov.turn(-1)
            elif path[0] == 'R':
                mov.turn(1)
        path = path[1:]
        # if we rebuild then we should update our map boxes if we found new boxes
        if rebuild:
            raw_to_map(raw_laser_scan())
            print_map()
            path = dijkstra(x, y)

# This function is used to print the map
def print_map():
    for i in range(0, 19):
        std = ""
        for j in range(0, 19):
            std += (str(map[j][i]) + " ")
        print(std)

# This function sets the robot direction to a specific value
# Used only when the robot is near the box
def rotate_to(rot):
    if rot - rotation == -1 or rot - rotation == 3:
        mov.turn(-1)
    elif rot - rotation == 1 or rot - rotation == -3:
        mov.turn(1)
    elif rot - rotation == -2 or rot - rotation == 2:
        mov.turn(-1)
        mov.turn(-1)

# This method moves the robot to all the boxes un the list, detects and writes
# their color, and returns the position of the yellow box or a message that ARTag was found
def findYellow():
    for i, box in enumerate(box_list_colored):
        if box[1] is None:
            rot_coord = next_to(box[0])
            move_by_path(rot_coord[0], False)
            rotate_to(rot_coord[1])
            box_color = colorDecoder()
            artag = cameraDecode()
            if artag == 1:
                box_list_colored[i] = (box_list_colored[i][0], box_color)
                if box_color == "yellow":
                    return box[0]
            else:
                return "found"

# This function returns the nearest position next to the box side
def next_to(coordinates_temp):
    # Find the distance to the left, right, top and bottom sides of a box
    l = len(dijkstra(coordinates_temp[0] - 2, coordinates_temp[1]))
    r = len(dijkstra(coordinates_temp[0] + 2, coordinates_temp[1]))
    t = len(dijkstra(coordinates_temp[0], coordinates_temp[1] - 2))
    b = len(dijkstra(coordinates_temp[0], coordinates_temp[1] + 2))
    # If we cannot go to this side, reset the distance
    if l == 0:
        l = 10000
    if r == 0:
        r = 10000
    if t == 0:
        t = 10000
    if b == 0:
        b = 10000
    # Return the best variant
    min_len = min(l, r, t, b)
    if min_len == l:
        return ((coordinates_temp[0] - 2, coordinates_temp[1]), 1)
    if min_len == r:
        return ((coordinates_temp[0] + 2, coordinates_temp[1]), 3)
    if min_len == t:
        return ((coordinates_temp[0], coordinates_temp[1] - 2), 2)
    if min_len == b:
        return ((coordinates_temp[0], coordinates_temp[1] + 2), 0)

# This function moves along the sides of a box to find an ARTag on it
def find_ARTag(coordinates_temp):
    # Here we find the nearest available sides of this box
    l = len(dijkstra(coordinates_temp[0] - 2, coordinates_temp[1]))
    r = len(dijkstra(coordinates_temp[0] + 2, coordinates_temp[1]))
    t = len(dijkstra(coordinates_temp[0], coordinates_temp[1] - 2))
    b = len(dijkstra(coordinates_temp[0], coordinates_temp[1] + 2))
    if l == 0:
        l = 10000
    if r == 0:
        r = 10000
    if t == 0:
        t = 10000
    if b == 0:
        b = 10000
    
    # Here we go to these sides chosing the optimal path
    min_len = min(l, r, t, b)
    while min_len < 10000:
        min_len = min(l, r, t, b)
        if min_len == l:
            l = 10000
            move_by_path((coordinates_temp[0] - 2, coordinates_temp[1]), False)
            rotate_to(1)
        elif min_len == t:
            t = 10000
            move_by_path((coordinates_temp[0], coordinates_temp[1] - 2), False)
            rotate_to(2)
        elif min_len == r:
            r = 10000
            move_by_path((coordinates_temp[0] + 2, coordinates_temp[1]), False)
            rotate_to(3)
        elif min_len == b:
            b = 10000
            move_by_path((coordinates_temp[0], coordinates_temp[1] + 2), False)
            rotate_to(0)
        artag = cameraDecode()
        # If we have found the ARTag, return
        if artag == 2:
            return


# main function
if __name__ == "__main__":
    laser = robot.getLaser()
    laser = laser.get('values')[40:len(laser.get('values')) - 40]

    rotation = 3

    # checking in which orientation we are
    # to turn our robot to the right rotation
    sumLeft = 0
    sumRight = 0
    for z in range(100, 110):
        sumRight += laser[z]
    for z in range(492, 502):
        sumLeft += laser[z]
    if (sumLeft < 3.5 and laser[math.ceil(len(laser) / 2)] < 0.5):
        rotation = 2
        mov.turn(1)
    elif (sumLeft > 3.5 and laser[math.ceil(len(laser) / 2)] > 0.5):
        rotation = 0
        mov.turn(-1)
    elif (sumLeft > 3.5 and laser[math.ceil(len(laser) / 2)] < 0.5):
        rotation = 1
        mov.turn(1)
        mov.turn(1)

    #initial scan of the map
    raw_to_map(raw_laser_scan())

    #go to the coordinates 2,2 to explore all map
    while get_box_amount() < 4:
        move_by_path((2, 2), True, True)

    #inserting boxes to the box_list_colored to go through them in the future
    for i in range(0, 19):
        for j in range(0, 19):
            if map[i][j] == 3:
                box_list_colored.append(((i, j), None))

    raw_to_map(raw_laser_scan())
    
    # if we founf 4 boxes
    if get_box_amount() >= 4:
        # go to the closest to the border box
        box_coord = get_side_box()
        next_to_point = next_to(box_coord)
        move_by_path(next_to_point[0], False)
        rotate_to(next_to_point[1])
        box_color = colorDecoder()
        artag = cameraDecode()
        # and update box color
        for i, box in enumerate(box_list_colored):
            if box[0][0] == box_coord[0] and box[0][1] == box_coord[1]:
                box_list_colored[i] = (box[0], box_color)

        # calculating global coordinates 
        # and starting Yellow box finding or ARTag finding 
        returnInfo = "found"
        if artag == 1:
            if box_color == "red":
                X = locY
                Y = locX
                returnInfo = findYellow()
            elif box_color == "yellow":
                Y = 18 - locY
                X = locX
                returnInfo = box_coord
            elif box_color == "blue":
                X = 18 - locX
                Y = locY
                returnInfo = findYellow()
            elif box_color == "green":
                X = 18 - locY
                Y = 18 - locX
                returnInfo = findYellow()
            if returnInfo != "found":
                find_ARTag(returnInfo)
        else:
            box_color = "yellow"

        # calculating final coordinates to go to the finish position
        final_coordinates = (0, 0)
        if box_color == "red":
            final_coordinates = (YArTag * 2 + 2, XArTag * 2 + 2)
        elif box_color == "yellow":
            final_coordinates = (XArTag * 2 + 2, 16 - YArTag * 2)
        elif box_color == "blue":
            final_coordinates = (16 - XArTag * 2, YArTag * 2 + 2)
        elif box_color == "green":
            final_coordinates = (16 - YArTag * 2, 16 - XArTag * 2)

        move_by_path(final_coordinates, False)
        exit(1)
