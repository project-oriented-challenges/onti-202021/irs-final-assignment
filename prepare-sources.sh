#!/bin/bash

solution_file="solution.tex"
solution_dir="src"
solution_file_path="${solution_dir}/${solution_file}"

tex_header='% This file generated automatically by prepare-sources.sh'

generate_tex_file()
{
  relative_path="$1"

  files=`cat src/sources-list.txt`

  echo ${tex_header}

  for i in ${files}; do
    displayed=`echo ${i} | sed "s#src/full/##" | sed 's#_#\\\\_#g'`
    echo '\subsection*{'${displayed}'}'

    res_py=`echo "${i: -3}" | grep ".py" | wc -l | sed 's/ *//g'`
    res_js=`echo "${i: -3}" | grep ".js" | wc -l | sed 's/ *//g'`
    res_ts=`echo "${i: -3}" | grep ".ts" | wc -l | sed 's/ *//g'`
    if [ "${res_py}" == "1" ]; then
      echo '\inputminted[fontsize=\footnotesize, breaklines]{python}{'${relative_path}${i}'}'
    elif [ "${res_js}" == "1" ]; then
      echo '\inputminted[fontsize=\footnotesize, breaklines, tabsize=2, obeytabs]{js}{'${relative_path}${i}'}'
    elif [ "${res_ts}" == "1" ]; then
      echo '\inputminted[fontsize=\footnotesize, breaklines, tabsize=2, obeytabs]{js}{'${relative_path}${i}'}'
    else
      echo '\inputminted[fontsize=\footnotesize, breaklines]{text}{'${relative_path}${i}'}'
    fi

    echo
  done

}

collect_sources()
{
  echo "Prepare a TeX file with source code"
  generate_tex_file "$1" | tee ${solution_file_path}
}

if [ -r ${solution_file_path} ]; then
  hf=`head -1 ${solution_file_path}`
  if [ "${hf}" == "${tex_header}" ]; then
	  collect_sources "$1"
  else
  	echo "Manually prepared ${solution_file} detected. Skip TeX file generation."
  fi
else
  collect_sources "$1"
fi
